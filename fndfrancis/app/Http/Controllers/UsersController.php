<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\person;
class UsersController extends Controller
{
    public function save_user(){
      // collect form data from the Request
      //dd(request()->all());
    //$params_list=request(['username','password','gender','country']);

      //save data to db
    $p=new person;
    $p->username=request('username');
    $p->password=request('password');
    $p->gender=request('gender');
    $p->country=request('country');
    $p->save();

      //take user to login page
      return redirect('/public/login');
    }

    public function authenticate(){
      $username=request('username');
      $password=request('password');
      $l=person::where('username',$username)->where('password',$password)->get();
      if(count($l)>0){
        session([
          'user' => $l[0]
        ]);
        return redirect('/home');
      }
      else {
        return redirect('/public/login');
      }
    }

    public function listusers(){
      $id=session('user') ->id;
      $people=person::where('id','!=',$id)->get();
      return view('users.list',compact('$people'));
    }

    public function logout(){
      session()->flush();
      return redirect('/public/login');
    }
}
