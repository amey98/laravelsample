@extends('layouts.master')

@section('title')
  register to find fndfrancis
@endsection

@section('header')
Register here
@endsection

@section('content')
<form method="post" action="/users" >
  {{ csrf_field()}};

  <div class="form-element">
    <input type="text" placeholder="Username" name="username">


  </div>
  <div class="form-element">
    <input type="password" placeholder="password" name="password">


  </div>

        <label>Gender</label>
        <div class="radio-container form-element">
            <span>
              <input type="radio" name="gender" id="male" value="M">
              <label for="male">MAle</label>

            </span>
            <span>
              <input type="radio" name="gender" id="female" value="F">
              <label for="female">FemAle</label>

            </span>

            <span>
              <input type="radio" name="gender" id="other" value="O">
              <label for="other">Other</label>

            </span>


        </div>
        <div class="contry-container form-element">
          <label>contry</label>
          <select name="country">
            @foreach ($countries as $country)
            <option>{{ $country }}</option>
            @endforeach
          </select>
        </div>


  <div class="action">
    <button type="submit">Register</button>

  </div>

</form>
@endsection
