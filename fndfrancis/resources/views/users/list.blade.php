@extends('layouts.master')

@section('title')
  your friends
@endsection
@section('header')
Welocome ({{session('user')->username}})&nbsp;
<a href="/logout">Logout</a>
@endsection



@section('content')
  <div class="menu-links">
    <a href="/users">Friends</a>
  </div>
  <div class="friends">
    <table>
      <tr>
        <th>username</th>
        <th>gender</th>
        <th>country</th>
      </tr>
      @foreach ($people as $person)
        <tr>
          <td>{{$person->username}}</td>
          <td>{{$person->gender}}</td>
          <td>{{$person->country}}</td>
        </tr>
      @endforeach
    </table>
  </div>
@endsection
